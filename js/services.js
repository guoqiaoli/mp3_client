// js/services/todos.js
angular.module('demoServices', [])
    .factory('Get', function($http, $window) {      
        return {
            getList : function(p) {
                var baseUrl = $window.sessionStorage.baseurl;
                return $http.get(baseUrl+"/"+p);
            },
            getOne : function(p1,p2){
                var baseUrl = $window.sessionStorage.baseurl;
                return $http.get(baseUrl+"/"+p1+"/"+p2);
            }
        }
    })
    .factory('Post', function($http, $window) {      
        return {
            post : function(p,obj) {
                var baseUrl = $window.sessionStorage.baseurl;
                return $http.post(baseUrl+"/"+p,obj);
            }
        }
    })

    .factory('Delete', function($http, $window) {      
        return {
            delete : function(p1,p2) {
                var baseUrl = $window.sessionStorage.baseurl;
                return $http.delete(baseUrl+"/"+p1+"/"+p2);
            }
        }
    })

    .factory('Put', function($http, $window) {      
        return {
            put : function(p1,p2,obj) {
                var baseUrl = $window.sessionStorage.baseurl;
                return $http.put(baseUrl+"/"+p1+"/"+p2,obj);
            }
        }
    })
    .factory('mySort', function($http, $window) {      
        return {
            sort : function mysort(method, order, task){
                if(method == "name"){
                    if(order == "inc"){
                        task.sort(function(a,b){if(a.name<b.name) return -1;if(a.name>b.name) return 1; return 0;})
                    }
                    else{
                        task.sort(function(a,b){if(a.name<b.name) return 1;if(a.name>b.name) return -1; return 0;})
                    }
                }
                else if(method == "deadline"){
                    if(order == "inc"){
                        task.sort(function(a,b){if(a.deadline<b.deadline) return -1;if(a.deadline>b.deadline) return 1; return 0;})
                    }
                    else{
                        task.sort(function(a,b){if(a.deadline<b.deadline) return 1;if(a.deadline>b.deadline) return -1; return 0;})
                    }
                }
                else{
                    if(order == "inc"){
                        task.sort(function(a,b){if(a.assignedUserName<b.assignedUserName) return -1;if(a.assignedUserName>b.assignedUserName) return 1; return 0;})
                    }
                    else{
                        task.sort(function(a,b){if(a.assignedUserName<b.assignedUserName) return 1;if(a.assignedUserName>b.assignedUserName) return -1; return 0;})
                    }
                }
                return task;
            }
        }
    })

    .factory('Filter', function($http, $window) {      
        return {
            fileter : function filter(f,t1,t2){
                        if(f=="all"){
                            t2=[];
                            for(var i=0; i<t1.length;i++){
                                t2.push(t1[i]);
                            }
                        }
                        else if(f=="pendingtask"){
                            t2=[];
                            for(var i=0; i<t1.length;i++){
                                if(t1[i].completed==false)
                                    t2.push(t1[i]);
                            }
                        }
                        else{
                            t2=[];
                            for(var i=0; i<t1.length;i++){
                                if(t1[i].completed==true)
                                    t2.push(t1[i]);
                            }
                        }
                    }
                 }
            })

    ;
