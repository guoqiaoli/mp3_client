// var demoApp = angular.module('demoApp', ['demoControllers']);

var demoApp = angular.module('demoApp', ['ngRoute', 'demoControllers', 'demoServices']);

demoApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
    when('/users', {
    templateUrl: 'partials/users.html',
    controller: 'FirstController'
  }).
  when('/tasks', {
    templateUrl: 'partials/tasks.html',
    controller: 'SecondController'
  }).
  when('/settings', {
    templateUrl: 'partials/settings.html',
    controller: 'SettingsController'
  }).
  when('/addtask', {
    templateUrl: 'partials/addtask.html',
    controller: 'AddtaskController'
  }).
  when('/adduser', {
    templateUrl: 'partials/adduser.html',
    controller: 'AdduserController'
  }).
  when('/updatetask/:task_id', {
    templateUrl: 'partials/updatetask.html',
    controller: 'UpdatetaskController'
  }).
  when('/userinfo/:user_id', {
    templateUrl: 'partials/userinfo.html',
    controller: 'UserinfoController'
  }).
  when('/taskinfo/:task_id', {
    templateUrl: 'partials/taskinfo.html',
    controller: 'TaskinfoController'
  }).
  otherwise({
    redirectTo: '/settings'
  });
}]);