var demoControllers = angular.module('demoControllers', []);

demoControllers.controller('FirstController', ['$scope', 'Get', "Delete", function($scope, Get, Delete) {
  // $scope.data=Get.get();
  Get.getList("users").success(function(data){
    $scope.data=data['data'];
  })

  $scope.del = function(id){
    var url = $scope.url + "/" +id;
    Delete.delete("users",id);

    for(user in $scope.data){
      if(id == $scope.data[user]._id){
        var tasks = $scope.data[user].pendingTasks;
        for(task in tasks){
          // var url2 = $window.sessionStorage.baseurl + "/tasks/" + tasks[task];
          Delete.delete("tasks",tasks[task]);
        }
        $scope.data.splice(user,1);
      }
    }
  };
}]);

demoControllers.controller('AdduserController', ['$scope','Post','Get',function($scope,Post,Get) {
  $scope.nameErr="";
  $scope.emailErr="";
  $scope.repeatEmail = false;
  $scope.users;
  $scope.message;

  // $http.get($scope.url).success(function(data, status, headers, config){
    $scope.users = Get.getList("users");
  // });

  $scope.a = function(){
    var flag = 1;
    if(! $scope.name){
      $scope.nameErr = "Please enter the name";
      flag = 0;
    }

    if(! $scope.email){
      $scope.emailErr = "Please enter the email";
      flag = 0;
    }

    if($scope.email.indexOf("@")<0 || $scope.email.indexOf(".")<0){
      $scope.emailErr = "This is not a valid email";
      flag=0;
    }
    for(user in $scope.users){
      if($scope.users[user].email==$scope.email){
        $scope.emailErr="This email already exist";
        flag=0;
      }
    }

    if(flag == 1){
      var obj = {
        name: $scope.name,
        email: $scope.email
      }

      Post.post("users",obj);
      $scope.message="User added!";
    }
  }
}]);

demoControllers.controller('UserinfoController', ['$scope', '$http' ,'$window', '$routeParams', function($scope, $http, $window, $routeParams) {
   $scope.user_id = $routeParams.user_id;
   $scope.url = $window.sessionStorage.baseurl+"/users/" + $scope.user_id;
   $scope.user;
   $scope.pendingTask=[];
   $scope.completeTask=[];
   $scope.message;
   $scope.show=false;

   $http.get($scope.url).success(function(data){
        $scope.user = data['data'][0];
        for(var i=0; i<$scope.user.pendingTasks.length;i++){
          var url1 = $window.sessionStorage.baseurl + "/tasks/" + $scope.user.pendingTasks[i];

          $http.get(url1).success(function(data){
            $scope.message = data['message'];

              var task = data['data'][0];
              if(task.completed){
                $scope.completeTask.push(task);
              }
              else{
                $scope.pendingTask.push(task);
              }
          });
        }
   });

   $scope.showOrhide = function(){
      $scope.show = ! $scope.show;
   }

   $scope.complete = function(id){
      var url1 = $window.sessionStorage.baseurl + "/tasks/" + id;
      $http.put(url1,{completed:true});

      for(task in $scope.pendingTask){
        if($scope.pendingTask[task]._id ==id) {
            $scope.completeTask.push($scope.pendingTask[task]);
            $scope.pendingTask.splice(task,1);
        }
      }
   }
}]);


demoControllers.controller('SecondController', ['$scope', '$http' ,'$window', function($scope, $http, $window) {
  $scope.url = $window.sessionStorage.baseurl+"/tasks";
  $scope.tasks;
  $scope.partialtasks=[];
  $scope.offset=0;
  $scope.filter="all";
  $scope.order="inc";
  $scope.intertasks=[];
  $scope.message="initial message";

  $scope.options=[
  {label:"name", value:"name"},
  {label:"deadline", value:"deadline"},
  {label:"assignedUserName",value:"assignedUserName"}];
  $scope.Selection = $scope.options[0];


  $http.get($scope.url).success(function(data, status, headers, config){
        $scope.tasks = data['data'];
        filter();
        mysort($scope.Selection.value,$scope.order);
        for(i=0; i<3; i++)
          if(i<$scope.intertasks.length)
            $scope.partialtasks.push($scope.intertasks[i]);
  });



  $scope.prev = function(){
    if($scope.offset>2){
    $scope.partialtasks.splice(0,$scope.partialtasks.length);
     $scope.offset -= 3;
      for(i=$scope.offset;i<$scope.offset+3;i++){
        if(i<$scope.intertasks.length)
          $scope.partialtasks.push($scope.intertasks[i]);
    }
  }
  }


  $scope.next = function(){
    if($scope.offset<$scope.intertasks.length-3){ 
      $scope.partialtasks.splice(0,$scope.partialtasks.length);
      $scope.offset += 3;
      for(i=$scope.offset;i<$scope.offset+3;i++){
        if(i<$scope.intertasks.length)
          $scope.partialtasks.push($scope.intertasks[i]);
      }
    }
  }


  $scope.del = function(id){
    var url = $scope.url + "/" + id;
    $http.delete(url);

    for(task in $scope.partialtasks){
       $scope.message = "am i here?";
      if($scope.partialtasks[task]._id == id) $scope.partialtasks.splice(task,1);
    }

    for(task in $scope.tasks){
      if($scope.tasks[task]._id == id){
        var user_id = $scope.tasks[task].assignedUser;
        if(user_id!=""){
          var url = $window.sessionStorage.baseurl + "/users/" + user_id;
          $http.put(url,{pendingTask:id});
          $scope.tasks.splice(task,1);}
      }
    }
    // filter();
    // change();
  }

  $scope.all = function(){
    $scope.partialtasks=[];
    $scope.filter="all";
    filter();
    mysort($scope.Selection.value, $scope.order);
    change();
  }

  $scope.pending = function(){
    $scope.partialtasks=[];
    $scope.filter="pendingtask";
    filter();
    mysort($scope.Selection.value, $scope.order);
    change();
  }

  $scope.completed = function(){
    $scope.partialtasks=[];
    $scope.filter="completedtask";
    filter();
    mysort($scope.Selection.value, $scope.order);
    change();
  }

  $scope.method = function(){
    $scope.partialtasks=[];
    mysort($scope.Selection.value, $scope.order);
    change();
  }

  $scope.inc = function(){
    $scope.order = "inc";
    $scope.partialtasks=[];
    mysort($scope.Selection.value, $scope.order);
    change();
  }

  $scope.dec = function(){
    $scope.order = "dec";
    $scope.partialtasks=[];
    mysort($scope.Selection.value, $scope.order);
    change();
  }

  function mysort(method, order){
    if(method == "name"){
      if(order == "inc"){
         $scope.intertasks.sort(function(a,b){if(a.name<b.name) return -1;if(a.name>b.name) return 1; return 0;})
      }
      else{
         $scope.intertasks.sort(function(a,b){if(a.name<b.name) return 1;if(a.name>b.name) return -1; return 0;})
      }
    }
    else if(method == "deadline"){
      if(order == "inc"){
         $scope.intertasks.sort(function(a,b){if(a.deadline<b.deadline) return -1;if(a.deadline>b.deadline) return 1; return 0;})
      }
      else{
         $scope.intertasks.sort(function(a,b){if(a.deadline<b.deadline) return 1;if(a.deadline>b.deadline) return -1; return 0;})
      }
    }
    else{
      if(order == "inc"){
         $scope.intertasks.sort(function(a,b){if(a.assignedUserName<b.assignedUserName) return -1;if(a.assignedUserName>b.assignedUserName) return 1; return 0;})
      }
      else{
         $scope.intertasks.sort(function(a,b){if(a.assignedUserName<b.assignedUserName) return 1;if(a.assignedUserName>b.assignedUserName) return -1; return 0;})
      }
    }
  }
  function filter(){
    if($scope.filter=="all"){
        $scope.intertasks=[];
        for(var i=0; i<$scope.tasks.length;i++){
          $scope.intertasks.push($scope.tasks[i]);
        }
    }
    else if($scope.filter=="pendingtask"){
      $scope.intertasks=[];
        for(var i=0; i<$scope.tasks.length;i++){
          if($scope.tasks[i].completed==false)
            $scope.intertasks.push($scope.tasks[i]);
        }
    }
    else{
      $scope.intertasks=[];
        for(var i=0; i<$scope.tasks.length;i++){
          if($scope.tasks[i].completed==true)
            $scope.intertasks.push($scope.tasks[i]);
        }
    }
  }

  function change(){
    for(var i = $scope.offset; i<$scope.offset+3; i++){
      if(i<$scope.intertasks.length)
        $scope.partialtasks.push($scope.intertasks[i]);
    }
  }
}]);

demoControllers.controller('AddtaskController', ['$scope', '$http' ,'$window', function($scope, $http, $window) {
  $scope.url1 = $window.sessionStorage.baseurl+"/users";
  $scope.url2 = $window.sessionStorage.baseurl+"/tasks";
  $scope.users="asdf";
  $scope.message="";
  $scope.options = [];

  $scope.Selection;
  $http.get($scope.url1).success(function(data, status, headers, config){
        $scope.users = data['data'];
        for(user in $scope.users){
          var obj={label: $scope.users[user].name, value: $scope.users[user]._id};
          $scope.options.push(obj);
        }
        $scope.Selection = $scope.options[0];
  });


  $scope.a = function(){
    var task ={name:$scope.name, deadline:$scope.deadline, assignedUser: $scope.Selection.value,assignedUserName: $scope.Selection.label, description:$scope.description};
    var flag = 1;
    if(! $scope.name){
      $scope.nameErr = "Name Required";
      flag = 0;
    } 

    if(! $scope.deadline){
      $scope.deadlineErr="Deadline Required";
      flag = 0;
    }

    if(flag == 1){ 
      $http.post($scope.url2,task).success(function(data, status, headers, config){
        var url = $scope.url1+"/"+$scope.Selection.value;
        var id = data['data']._id;
        $scope.message = url;
        $http.put(url,{pendingTask:id});
        $scope.message="Task added!";
      });
    }
  }
}]);


demoControllers.controller('TaskinfoController', ['$scope',  '$http' ,'$window', '$routeParams', function($scope,  $http, $window, $routeParams) {
    $scope.task_id = $routeParams.task_id;
    $scope.url =  $window.sessionStorage.baseurl+"/tasks/" +$routeParams.task_id;
    $scope.task;
    $http.get($scope.url).success(function(data){
      $scope.task = data['data'][0];
    }); 
}]);

demoControllers.controller('UpdatetaskController', ['$scope', '$http' ,'$window', '$routeParams', function($scope,  $http, $window, $routeParams) {
    $scope.task_id = $routeParams.task_id;
    $scope.url =  $window.sessionStorage.baseurl+"/tasks/" +$routeParams.task_id;
    $scope.task;
    $scope.olduser;
    $scope.message="";

     $scope.options = [];

    $scope.Selection;
    $http.get($window.sessionStorage.baseurl+"/users").success(function(data, status, headers, config){
        $scope.users = data['data'];
        for(user in $scope.users){
          var obj={label: $scope.users[user].name, value: $scope.users[user]._id};
          $scope.options.push(obj);
        }
        $scope.Selection = $scope.options[0];
    });


    //get the old user own this task
    $http.get($scope.url).success(function(data){
      $scope.olduser=data['data'][0].assignedUser;
    });
    
    $scope.a = function(){
      var t ={name:$scope.name, deadline:$scope.deadline, assignedUser: $scope.Selection.value,assignedUserName: $scope.Selection.label, description:$scope.description};
      //update task
      $http.put($scope.url,t);
      //update user 
      var url1=$window.sessionStorage.baseurl + "/users/" + $scope.Selection.value;
      $http.put(url1, {pendingTask:$scope.task_id});
      //delete old user
      var url2=$window.sessionStorage.baseurl + "/users/" + $scope.olduser;
      $http.put(url2,{pendingTask:$scope.task_id});
      $scope.message="Updated!";
    }
}]);

demoControllers.controller('SettingsController', ['$scope' , '$window' , function($scope, $window) {
  $scope.url = $window.sessionStorage.baseurl;

  $scope.setUrl = function(){
    $window.sessionStorage.baseurl = $scope.url; 
    $scope.displayText = $window.sessionStorage.baseurl;
  };

}]);


